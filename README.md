### 数据结构 

表名|说明|类型|相关操作
---|---|---|---
fds:[fd]|fd所在的房间集合|集合Set|系统管理
rooms:[rid]|房间内的fd集合|集合Set|系统管理
player:[fd]|玩家信息|字符串|
room:list|房间集合 当前存在的房间|集合Set|SADD key member1 [member2] 向集合添加一个或多个成员<br> SISMEMBER key member 判断 member 元素是否是集合 key 的成员<br> SREM key member1 [member2] 移除集合中一个或多个成员
room:info:[rid]|房间信息表|哈希Hash| HMSET key field1 value1 [field2 value2 ] 同时将多个 field-value (域-值)对设置到哈希表 key 中。
room:log:[rid]|房间记录表|列表|RPUSH key value1 [value2] 在列表中添加一个或多个值<br>LRANGE key start stop 获取列表指定范围内的元素


### room:info:[rid] 房间信息
字段|说明|值
---|---|---
roomId|房间号|
type|游戏类型|1:FiveChess
state|游戏状态|0:等待玩家加入 1:等待其他人准备 2:游戏进行中 3:游戏结束
round|游戏回合|0
count|游戏局数|0


### 流程
1. 网络连接时,客户端进行登录操作 设置|获取 player:[fd] 信息
2. 创建房间时
2.1 创建房间时 room:list 先判断房间号是否存在 不存在则添加入集合中
2.2 更新player:fd的roomId信息
2.3 将player:fd join入房间
2.4 生成 room:info:[rid] 的房间信息

