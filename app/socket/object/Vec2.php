<?php

namespace app\socket\object;

/**
 * V2向量
 */
class Vec2
{
    public $x = 0;
    public $y = 0;
    public $mx = 0;
    public $my = 0;
    /**     
     *
     * @param int $x
     * @param int $y
     */
    public function __construct($x, $y, $mx, $my)
    {
        $this->x = $x;
        $this->y = $y;
        $this->mx = $mx;
        $this->my = $my;
    }

    /**
     * 当前向量的上
     *
     * @return Vec2|bool
     */
    public function up()
    {
        $y = $this->y - 1;
        if ($y < 0) {
            return false;
        }
        return new Vec2($this->x, $y, $this->mx, $this->my);
    }
    /**
     * 当前向量下
     *
     * @return Vec2
     */
    public function down()
    {
        $y = $this->y + 1;
        if ($y >= $this->my) {
            return false;
        }
        return new Vec2($this->x, $y, $this->mx, $this->my);
    }
    /**
     * 当前向量的左
     *
     * @return Vec2
     */
    public function left()
    {
        $x = $this->x - 1;
        if ($x < 0) {
            return false;
        }
        return new Vec2($x, $this->y, $this->mx, $this->my);
    }
    /**
     * 当前向量的
     *
     * @return Vec2
     */
    public function right()
    {
        $x = $this->x + 1;
        if ($x >= $this->mx) {
            return false;
        }
        return new Vec2($x, $this->y, $this->mx, $this->my);
    }
    /**
     * 左上
     *
     * @return Vec2
     */
    public function leftUp()
    {
        $x = $this->x - 1;
        $y = $this->y - 1;
        if ($x < 0 || $y < 0) {
            return false;
        }
        return new Vec2($x, $y, $this->mx, $this->my);
    }
    /**
     * 右上
     *
     * @return Vec2
     */
    public function rightUp()
    {
        $x = $this->x + 1;
        $y = $this->y - 1;
        if ($x >= $this->mx || $y < 0) {
            return false;
        }
        return new Vec2($x, $y, $this->mx, $this->my);
    }
    /**
     * 左下
     *
     * @return Vec2
     */
    public function leftDown()
    {
        $x = $this->x - 1;
        $y = $this->y + 1;
        if ($x < 0 || $y >= $this->my) {
            return false;
        }
        return new Vec2($x, $y, $this->mx, $this->my);
    }
    /**
     * 右下
     *
     * @return Vec2
     */
    public function rightDown()
    {
        $x = $this->x + 1;
        $y = $this->y + 1;
        if ($x >= $this->mx || $y >= $this->my) {
            return false;
        }
        return new Vec2($x, $y, $this->mx, $this->my);
    }

    public function __toString()
    {
        return "x:" . $this->x . " " . "y:" . $this->y;
    }
}
