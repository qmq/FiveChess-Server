<?php

namespace app\socket;

use app\socket\types\FcLog;
use app\socket\types\FcPlayer;
use app\socket\types\FcRoom;
use app\socket\types\Player;

class FiveChess extends Base
{
    /**
     * 创建房间
     *
     * @return void
     */
    public function onCreateRoom()
    {
        //当前用户的Fd
        $fd = $this->websocket->getSender();

        $original = Player::find($fd);
        $original->type = 1;
        $original->save();

        $player = FcPlayer::find($fd);
        $roomId = FcRoom::getNotExistRoom();

        $room = new FcRoom();
        $room->roomId = $roomId;
        $room->state = 0;
        $room->add($player);

        $room->save();
        $player->save();

        $this->websocket->join($roomId);
        $this->websocket->emit("IntoRoom", [
            'gameRoom' => $room,
            'player' => $player,
            "block" => $room->blockPlayer,
            "white" => $room->whitePlayer,
            'obPlayers' => $room->obPlayers,
            'logs' => []
        ]);
    }

    /**
     * 某人加入房间
     *
     * @param array $data 
     * @param $data $roomId 房间号ID
     * @return void
     */
    public function onJoinRoom($data)
    {

        $fd = $this->websocket->getSender();
        $roomId = "r" . trim($data['roomId'], "r");

        $original = Player::find($fd);
        $original->type = 1;
        $original->save();

        $room = FcRoom::find($roomId);
        $player = FcPlayer::find($fd);
        $room->add($player);
        $player->save();

        if ($player->camp != 2 && $room->block && $room->white) {
            //加入的是一个玩家            
            $room->state = 1;
        }


        $room->save();



        $this->websocket->join($roomId)->emit('IntoRoom', [
            "gameRoom" => $room,
            "player" => $player,
            "block" => $room->blockPlayer,
            "white" => $room->whitePlayer,
            'obPlayers' => $room->obPlayers,
            'logs' => FcLog::find($roomId)->items
        ]);



        //给房间内的其他人发送 有人加入的信息
        $this->websocket->broadcast()->to($roomId)->emit("JoinRoom", $player);
    }

    /**
     * 重新开局||准备
     *
     * @return void
     */
    public function onReady()
    {
        $fd = $this->websocket->getSender();
        $player = FcPlayer::find($fd);
        $player->ready = 1;
        $player->save();

        $room = FcRoom::find($player->roomId);
        $room->add($player);


        if ($room->blockPlayer->ready && $room->whitePlayer->ready) {
            //黑白玩家都准备好了
            $room->state = 2;
            $room->count += 1;
            $room->round = 0;
            $room->logger->delete();
            $room->save();
            $this->websocket->to($room->roomId)->emit("FirstRound", ['current' => $room->block]);
        } else {
            $room->state = 1;
            $this->websocket->broadcast()->to($room->roomId)->emit("Ready", ['current' => $fd]);
        }
    }

    /**
     * 回合结束 
     * 当前客户端回合结束 落子位置  对方回合开始
     * 通知房间内的其他人 下个回合执行人 以及当前客户端的落子位置
     * @param int $index 当前客户端的落子位置
     * @return void
     */
    public function onDownChess($index)
    {
        $fd = $this->websocket->getSender();
        $player = FcPlayer::find($fd);
        $room = FcRoom::find($player->roomId);
        $room->round += 1;
        $room->logger->log($player, $index);
        $room->save();


        // //通知房间内的其他人当前客户端落子了
        $this->websocket->broadcast()->to($room->roomId)->emit("DownChess", [
            'current' => $fd,
            'index' => $index
        ]);
    }

    /**
     * 当前客户端胜利
     *
     * @return void
     */
    public function onWin($data = [])
    {

        $fd = $this->websocket->getSender();
        $player = FcPlayer::find($fd);
        $room = FcRoom::find($player->roomId);
        $room->add($player);

        $room->blockPlayer->ready = 0;
        $room->whitePlayer->ready = 0;
        $room->win = $fd;
        $room->state = 3;

        $room->save();
        $room->blockPlayer->save();
        $room->whitePlayer->save();

        //通知房间内的所有人 某客户端获取胜利
        $data['current'] = $fd;
        $this->websocket->to($room->roomId)->emit("Win", $data);
    }

    /**
     * 客户端请求悔棋
     *
     * @return void
     */
    public function onRegret()
    {
        //通知房间内的其他人,RegretTip
        $fd = $this->websocket->getSender();
        $this->websocket->broadcast()->emit("RegretTip", ["current" => $fd]);
    }

    /**
     * 对悔棋请求的确认
     * yes no
     *
     * @return void
     */
    public function onSureRegret($data)
    {
        $fd = $this->websocket->getSender();
        $player = FcPlayer::find($fd);
        $res['cmd'] = 0; //不可以悔棋
        $res['fd'] = $fd;
        $res['delete'] = [];
        if ($data) {
            $room = FcRoom::find($player->roomId);
            //$fd 是同意对方悔棋的人
            $res['cmd'] = 1;
            list($delete, $cross) = $room->backChess($fd);
            $res['cross'] = $cross;
            $res['delete'] = $delete;
        }
        $this->websocket->to($player->roomId)->emit("SureBackChess", $res);
    }
}
