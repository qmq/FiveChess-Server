<?php

namespace app\socket\types;

use app\socket\object\Game;

/**
 * 五子棋玩家类
 * @property int $fd
 * @property string $name 名称
 * @property string $avatar 头像
 * @property string $roomId 房间号
 * @property int $camp 阵营 0:黑棋 1:白棋 2:观看者
 * @property bool $ready 是否已准备好
 */
class FcPlayer extends Player
{
    public $pk = "fd";
    public $cacheKey = ["swoole", "fc", "player"];

    public static function find($fd)
    {
        $player = parent::find($fd);

        if (!$player->fd) {
            $key = implode(":",  ["swoole", "player", $fd]);
            $data = $player->redis->hgetall($key);
            $player->data($data);
        }
        return $player;
    }
}
