<?php

namespace app\socket\types;

/**
 * 扫雷玩家
 * 
 * @property bool $owner 是否是房主
 * @property int $sort 顺序
 */
class SwPlayer extends Player
{
    public $pk = "fd";
    public $cacheKey = ["swoole", "sw", "player"];


    /**
     *
     * @param int $fd
     * @return SwPlayer
     */
    public static function find($fd)
    {
        $player = parent::find($fd);

        if (!$player->fd) {
            $key = implode(":",  ["swoole", "player", $fd]);
            $data = $player->redis->hgetall($key);
            $player->data($data);
        }
        return $player;
    }
}
