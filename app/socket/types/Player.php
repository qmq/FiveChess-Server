<?php

namespace app\socket\types;

use app\socket\object\Game;
use app\socket\object\Name;
use think\helper\Arr;

/**
 * 玩家基类
 * @property int $fd
 * @property string $name 名称
 * @property string $avatar 头像
 * @property string $roomId 房间号 
 * @property int $type 游戏类型 1:五子棋 2:扫雷
 */
class Player extends Game
{
    public $pk = "fd";
    public $cacheKey = ["swoole", "player"];


    public static function create($fd)
    {
        $player = new Player();
        $player->fd = $fd;
        $player->name = (new Name())->getName();
        $player->avatar = Arr::random(["01", "02"]);
        return $player;
    }
}
