<?php

namespace app\socket;

use app\socket\object\Mine;
use app\socket\types\Player;
use app\socket\types\SwPlayer;
use app\socket\types\SwRoom;

class Sweeping extends Base
{

    /**
     * 创建扫雷房间
     *
     * @return void
     */
    public function onCreateSroom()
    {
        //当前用户的Fd
        $fd = $this->websocket->getSender();
        //新的房间号        
        $original = Player::find($fd);
        $original->type = 2;
        $original->save();

        $roomId = SwRoom::getNotExistRoom();

        $player = SwPlayer::find($fd);
        $player->owner = true;
        $player->roomId = $roomId;

        //将当前玩家加入到某房间中
        $this->websocket->join($roomId);

        $room = new SwRoom();
        $room->roomId = $roomId;
        $room->state = 0;
        $room->round = 0;
        $room->count = 0;
        $room->add($player);

        $player->save();
        $room->save();

        $this->websocket->emit("IntoSroom", [
            'gameRoom' => $room,
            'player' => $player
        ]);
    }




    /**
     * 加入扫雷房间
     *
     * @return void
     */
    public function onJoinSroom($roomId)
    {
        $roomId = "r" . trim($roomId, "r");
        $fd = $this->websocket->getSender();

        $original = Player::find($fd);
        $original->type = 2;
        $original->save();


        $player = SwPlayer::find($fd);
        $player->roomId = $roomId;
        $this->websocket->join($roomId);


        $room = SwRoom::find($roomId);
        $room->add($player);
        if (count($room->fds) == 1) {
            $player->owner = true;
        } else {
            $player->owner = false;
        }



        $player->save();
        $room->save();
        $this->websocket->emit("IntoSroom", [
            'gameRoom' => $room,
            'player' => $player,
            'otherPlayer' => $room->getOtherPlayers($fd)
        ]);

        $this->websocket->broadcast()->to($roomId)->emit("JoinSroom", $player);
    }

    /**
     * 扫雷开始
     * 创建地图
     *
     * @return void
     */
    public function onStartSweeping($type)
    {
        $fd = $this->websocket->getSender();
        $player = SwPlayer::find($fd);
        $room = SwRoom::find($player->roomId);

        $mine = new Mine();
        $mine->createMap($type);
        $map = $mine->getMap();

        $room->state = 1;
        $room->map = $map;
        $room->mapType = $mine->getMineType();
        $room->fd = $fd;
        $room->round = 0;
        $room->count += 1;
        $room->open = [];
        $room->save();

        $this->websocket->to($room->roomId)->emit("StartSweeping", [
            'gameRoom' => $room->hidden(['map'])
        ]);
    }
    /**
     * 格子点击
     *
     * @param int $index
     * @return void
     */
    public function onMineTiledClick($index)
    {
        $fd = $this->websocket->getSender();
        $player = SwPlayer::find($fd);
        $room = SwRoom::find($player->roomId);
        $next = $room->next($fd); //下一个回合要执行的人
        $room->fd = $next;
        $room->round += 1;
        $mine = Mine::create($room->map, $room->mapType);
        /**
         * close 关闭的结点 即点开后是数字N的格子
         * open 开放的结点  即点开后是安全区
         * current 当前点击格子的情况 -1:代表雷 0代表安全 1~8代表周边有N个雷
         */
        $info = $mine->aStart($index);
        $info['index'] = $index; //当前点击的格子
        $info['fd'] = $fd; //当前点击的人
        $info['next'] = $next;

        $room->log($info);

        /**
         * 游戏结束
         */
        if ($info['current'] == -1) {
            $room->state = 2;

            //找出其他雷的位置
            $info['mine'] = [];
            foreach ($room->map as $point => $value) {
                if ($value == 1 && $point != $index) {
                    array_push($info['mine'], $point);
                }
            }
        }

        $player->save();
        $room->save();



        $this->websocket->to($player->roomId)->emit("MineTiledClick", $info);
    }

    /**
     * 队友请求重新开局
     *
     * @return void
     */
    public function onResetSw()
    {
        $fd = $this->websocket->getSender();
        $player = SwPlayer::find($fd);
        $this->websocket->broadcast()->to($player->roomId)->emit("ResetSw", [
            'current' => $fd
        ]);
    }
}
