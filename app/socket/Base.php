<?php

namespace app\socket;

use think\Container;
use think\swoole\Websocket;

class Base
{
    /**
     * 
     *
     * @var Websocket
     */
    protected $websocket = null;


    public function __construct()
    {
        $this->websocket = app(Websocket::class);
    }
}
