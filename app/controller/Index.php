<?php

namespace app\controller;

use app\BaseController;
use app\socket\types\Mine;
use app\socket\types\Player;
use think\Cache;
use think\helper\Arr;

class Index extends BaseController
{
    public function index()
    {
        $a = [0, null, 1];

        $a = array_filter($a, function ($item) {
            return is_numeric($item);
        });

        dump($a);

        return;

        $roomId = $this->request->get("roomId");
        if (!$roomId) return;
        $redis = app(Cache::class);

        $room = $redis->HGETALL("swoole:room:info:r$roomId");



        $map = json_decode($room['map'], true);
        $mine = Mine::create($map, 1);

        $k = 0;
        foreach ($mine->getV2Map() as  $item) {
            foreach ($item as  $v) {
                if ($k <= 9) {
                    $k = "0" . $k;
                }

                //echo $k . "[" . $v . "]" . " ";
                echo $v . " ";
                $k++;
            }
            echo PHP_EOL;
        }
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
}
